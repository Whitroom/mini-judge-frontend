import { create, NButton, NCard, NRadioGroup, NRadioButton, NModal, NSpace, 
    NInput, NMenu, NIcon, NNotificationProvider, NMessageProvider, NDivider,
    NCarousel, NEllipsis, NDropdown, NCollapse, NCollapseItem, NTag, NSelect,
    NDataTable
} from "naive-ui";

const components = [ NButton, NCard, NRadioGroup, NRadioButton, NModal, NSpace, 
    NInput, NMenu, NIcon, NNotificationProvider, NMessageProvider, NDivider, NCarousel,
    NEllipsis, NDropdown, NCollapse, NCollapseItem, NTag, NSelect, NDataTable]

const naive = create({
    components
})

export default naive;