const globalUrl = "http://localhost:8000/"

const formHeaders = {
    "Content-Type": "multipart/form-data",
    'access-control-allow-credentials': 'true',
    "Access-Control-Allow-Headers": "Content-Type,XFILENAME,XFILECATEGORY,XFILESIZE"
}

const normalHeaders = {
    "accept": 'application/json',
    "Content-Type": "application/json",
    'access-control-allow-credentials': 'true',
    "Access-Control-Allow-Headers": "Content-Type,XFILENAME,XFILECATEGORY,XFILESIZE"
}

const verifyHeaders = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Headers": "Content-Type,XFILENAME,XFILECATEGORY,XFILESIZE",
    'access-control-allow-credentials': 'true',
    'accept': 'application/json',
    'Authorization': localStorage.Authorization
}

const uploadHeaders = {
    "accept": 'application/json',
    'access-control-allow-credentials': 'true',
    'Authorization': localStorage.Authorization
}

export { globalUrl, formHeaders, normalHeaders, verifyHeaders, uploadHeaders };