import axios from "axios";
import { normalHeaders, globalUrl } from '@/utils/global'
import { problem } from '@/models/status'

async function toGetStatus(): Promise<problem[] | null> {
    try {
        const res = await axios.get(globalUrl + 'sql/tests/gets', {headers: normalHeaders})
        const data: problem[] = []
        if (res.status == 200 && res.data.code == 0) {
            for (let i = 0; i < res.data.tests.length; i++){
                data.push({
                    key: res.data.tests[i].id,
                    commitNo: res.data.tests[i].id,
                    account: res.data.tests[i].users_id,
                    questionNo: res.data.tests[i].questions_id + 1000,
                    status: res.data.tests[i].correct_rate == 1 ? '答案正确' : '答案错误',
                    language: res.data.tests[i].language,
                    timeConsume: String(res.data.tests[i].time) + 'ms',
                    storageConsume: String(res.data.tests[i].memory/(1024*1024)^0) + 'M',
                    commitTime: res.data.tests[i].submit_time.split('T').join(' '),
                })
            }
            return data;
        } else {
            return null;
        }
    } catch (error) {
        console.log(error);
        return null
    }
}

export { toGetStatus }