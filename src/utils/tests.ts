import { testsCreate } from "@/models/tests";
import axios from "axios";
import { globalUrl, verifyHeaders } from "./global";

async function toUploadTest(create: testsCreate): Promise<any | null>{
    const options = ['text/x-csrc', 'text/x-c++src', 'text/x-python', 'text/x-java']
    const langs = ['C', 'C++', 'Python', 'Java'];
    create.language = langs[options.indexOf(create.language)]
    console.log(create);
    try {
        const res = await axios.post(globalUrl + 'sql/tests/create', create, {headers: verifyHeaders});
        if (res.status == 200 && res.data.code == 0) {
            console.log(res.data);
            return res.data;
        } else {
            return res.data;
        }
    } catch (error) {
        console.log(error);
        return null;
    }
}

export { toUploadTest }