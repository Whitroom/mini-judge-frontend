import axios from "axios";
import { globalUrl, normalHeaders } from '@/utils/global'

interface Classes{
    id: number;
    label: string;
    value: string;
    disabled: boolean
}

async function getClasses(): Promise<Classes[]> {
    const res = await axios.get(globalUrl + 'sql/classes/gets?limit=100', {headers: normalHeaders});
    const classes: Classes[] = [];
    if (res.status == 200 && res.data.code == 0){
        for(let i = 0; i < res.data.classes_.length; i++){
            classes.push({
                id: res.data.classes_[i].id,
                label: res.data.classes_[i].name,
                value: res.data.classes_[i].name,
                disabled: Boolean(!res.data.classes_[i].enabled)
            });
        }
    }
    return classes;
}

export { getClasses }