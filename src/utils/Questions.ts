import { problems } from '@/models/questions';
import { globalUrl, normalHeaders, uploadHeaders } from '@/utils/global'
import axios from 'axios'

async function checkSameQuestion(questionName: string): Promise<number>{
    const config = {
        headers: normalHeaders
    };
    try{
        console.log(questionName)
        const res = await axios.get(globalUrl + 'sql/questions/check?question_name=' + questionName, config);
        if (res.status == 200 && res.data.code == 1) {
            return 0;
        } else {
            return 1;
        }
    } catch(error) {
        console.log(error);
        return 2;
    }
}

async function toCreateQuestion(question_name: string, description:string, categoriesID: number, difficulty: string): Promise<number> {
    const form = new FormData();
    form.append('name', question_name);
    form.append('description', description);
    form.append('categories_id', String(categoriesID));
    form.append('difficulty', difficulty);
    try {
        const res = await axios.post(globalUrl + 'sql/questions/create', form, {headers: uploadHeaders});
        if (res.status == 200 && res.data.code == 0) {
            console.log(res.data)
            return res.data.question;
        } else {
            return 0;
        }
    } catch(error) {
        console.log(error);
        return -1;
    }

}

async function getTags(){
    const config = {
        headers: normalHeaders
    };
    try{
        const data: any[] = []
        const res = await axios.get(globalUrl + 'sql/catogories/gets?limit=100', config);
        if (res.status == 200 && res.data.code == 0) {
            console.log(res.data.catogories_)
            res.data.catogories_.forEach(element => {
                data.push({
                    label: element.name,
                    value: element.id
                })
            });
            return data;
        } else {
            return []
        }
    } catch(error) {
        console.log(error);
        return []
    }
}

async function toDeleteQuesiton(question_name: string): Promise<number> {
    console.log(question_name);
    const config = {
        headers: normalHeaders
    };
    const data = {
        name: question_name
    }
    try {
        const res = await axios.post(globalUrl + 'sql/questions/delete', data, config);
        if (res.status == 200 && res.data.code > 0){
            return res.data.code;
        } else {
            return 1;
        }
    } catch (error) {
        return 2;
    }
}

async function toGetQuestions(): Promise<problems[] | null> {
    try {
        const res = await axios.get(globalUrl + 'sql/questions/gets');
        const data: problems[] = [];
        if (res.status == 200 && res.data.code == 0){
            for(let i = 0; i < res.data.questions.length; i++){
                data.push({
                    key: res.data.questions[i].Questions.id,
                    problemNo: res.data.questions[i].Questions.question_id,
                    titleName: res.data.questions[i].Questions.name,
                    difficulty: res.data.questions[i].Questions.difficulty,
                    tags: res.data.questions[i].Categories.name
                });
            }
            return data;
        } else {
            return null;
        }
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function toGetQuestion(questionName: string): Promise<any | null> {
    try {
        console.log(questionName);
        const res = await axios.get(globalUrl + 'sql/questions/get', {params: {question_name: questionName}, headers: normalHeaders});
        if (res.status == 200 && res.data.code == 0){
            const data = res.data.question;
            return data;
        } else {
            return null;
        }
    } catch (error) {
        console.log(error);
        return null;
    }
}

export { checkSameQuestion, getTags, toCreateQuestion, toDeleteQuesiton, toGetQuestions, toGetQuestion }