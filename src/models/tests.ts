interface testsCreate {
    submit_time: number;
    question_id: number;
    language: string;
    code: string;
}

export { testsCreate }