interface contest {
    key: number;
    contestNo: number;
    titleName: string;
    status: string;
    startTime: string;
    endTime: string;
}

interface contest_questions {
    index: number;
    questionNo: number;
    questionInside: string;
    questionName: string;
    difficulty: string;
}

export { contest, contest_questions }