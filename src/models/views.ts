interface contestsViews {
    contest_id: number;
    name: string;
}

interface questionsViews {
    question_id: number;
    name: string;
    difficulty: string;
}

export { contestsViews, questionsViews }