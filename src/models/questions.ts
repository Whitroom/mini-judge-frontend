import { VNode, RendererNode, RendererElement } from 'vue'

interface Row {
    problemNo: number;
    titleName: string;
    difficulty: string;
    tags: string;
  }

interface columns {
    title: string;
    key: string;
    align: string;
    render(row: Row): VNode<RendererNode, RendererElement, {
        [key: string]: any;
    }>
}

interface problems {
    key: number;
    problemNo: number;
    titleName: string;
    difficulty: string;
    tags: string;
}

export { Row, columns, problems }