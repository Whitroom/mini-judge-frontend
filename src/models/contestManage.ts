interface classesOptions{
    label: string;
    value: number;
    disabled?: boolean;
}

interface questionsOptions {
    label: string;
    value: number;
    disabled?: boolean;
}

interface createForm {
    contestName: string;
    range: number;
    type: number;
    questions: Array<string | number>;
    classes: Array<string | number>;
}

export { classesOptions, questionsOptions, createForm }