interface problem {
    key: number;
    commitNo: number;
    account: string;
    questionNo: string;
    status: string;
    language: string;
    timeConsume: string;
    storageConsume: string;
    commitTime: string;
}

interface Row{
    commitNo: number;
    account: string;
    questionNo: string;
    language: string;
    status: string;
    timeConsume: string;
    storageConsume: string;
    commitTime: string;
  }

export { problem, Row }