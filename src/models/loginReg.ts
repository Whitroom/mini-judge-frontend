interface User{
    id: number;
    number: string
    password: string;
    classes: string[];
    name: string;
    sex: number;
    enabled: number;
    role: string;
}

export { User }