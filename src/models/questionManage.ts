
interface uploadQuestion {
    questionName: string;
    questionNo: number;
    tags: null | number;
    difficulty: null | number;
    editOnline: boolean;
    diff: boolean;
    description: string;
    files: {
        description: {
            exist: boolean;
        };
        data: {
            length: number;
        };
    };
    uploadDescription: boolean;
    uploadData: boolean;
}

interface deleteQuestion {
    questionName: string;
    canDelete: boolean;
}

export {uploadQuestion, deleteQuestion}