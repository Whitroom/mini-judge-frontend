import { createStore, Store } from 'vuex'
import { InjectionKey } from 'vue'

export interface State{
    contestNo: number,
    user: {
        account: string,
        name: string,
        sex: string,
        _class: string[],
        role: string
      },
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
    state: {
        contestNo: 0,
        user: {
            account: '',
            name: '',
            sex: '',
            _class: [''],
            role: ''
          },
    },
    mutations: {
        setContestNo(state, contestNo: number){
            state.contestNo = contestNo;
        },
        setUser(state, user: State['user']){
            state.user = user
        }
    }
})