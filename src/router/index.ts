import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/questions',
    name: 'Questions',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Questions.vue')
  },
  {
    path: '/questions//:questionName',
    name: 'question',
    component: () => import('../views/everyone/Question.vue')
  },
  {
    path: '/status',
    name: 'Status',
    component: () => import('../views/Status.vue')
  },
  {
    path: '/contests',
    name: 'Contests',
    component: () => import('../views/Contests.vue')
  },
  {
    path: '/contests/:contestid(\\d+)',
    name: 'contest',
    component: () => import('../views/everyone/Contest.vue')
  },
  {
    path: '/contests/:contestid(\\d+)/:questionName',
    name: 'contestQuestion',
  component: () => import('../views/everyone/Question.vue')
  },
  {
    path: '/rank',
    name: 'Rank',
    component: () => import('../views/Rank.vue')
  },
  {
    path: '/manage/contest',
    name: 'contestManage',
    component: () => import('../views/ContestManage.vue')
  },
  {
    path: '/manage/question',
    name: 'questionManage',
    component: () => import('../views/QuestionManage.vue')
  },
  { 
    path: '/:pathMatch(.*)*', 
    name: 'NotFound', 
    component: () => import('../views/NotFound.vue') 
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
