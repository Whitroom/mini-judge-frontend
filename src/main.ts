import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { store, key } from './vuex'
import 'vfonts/Lato.css'
import 'vfonts/FiraCode.css'
import naive from '@/plugins/naive-ui'
import VMEditor from '@kangc/v-md-editor'
import createKatexPlugin from '@kangc/v-md-editor/lib/plugins/katex/cdn';
import VMdPreview from '@kangc/v-md-editor/lib/preview'
import '@kangc/v-md-editor/lib/style/base-editor.css'
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js'
import '@kangc/v-md-editor/lib/theme/style/github.css'
import hljs from 'highlight.js'

VMEditor.use(githubTheme, {
    Hljs: hljs
});

VMdPreview.use(githubTheme, {
    Hljs: hljs,
})

VMEditor.use(createKatexPlugin())
VMdPreview.use(createKatexPlugin())

const app = createApp(App).use(naive).use(router).use(store, key).use(VMEditor).use(VMdPreview).mount('#app');
export default app;